<?php get_header();?>
        <div class="main-content-inner">
            <div class="container">
                <div class="row">

                    <!-- Cari Motor start -->
                    <div class="col-lg-8 mt-5 katalog">
                        <?php
                            // Setup your custom query
                            $args = array( 'post_type' => 'product');
                            $loop = new WP_Query( $args );
                        ?>
                        <div class="card">
                            <div class="card-body">
                                <h4 class="header-title">Katalog</h4>
                
                                <div class="row">
                                    <?php while ( $loop->have_posts() ) : $loop->the_post();?>
                                        <?php $product = new WC_Product(get_the_id());?>
                                        <div class="col-md-3">
                                            <div class="card">
                                                <div class="card-body">
                                                    <a class="link-as-text" href="<?php the_permalink();?>">
                                                        <h4 class="header-title"><?= $product->get_name();?></h4>
                                                        <div class="img-responsive">
                                                            <img src="<?= get_the_post_thumbnail_url(get_the_id());?>" />
                                                        </div>
                                                        <?= $product->get_price_html();?><br>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endwhile;?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Cari Motor end -->
                    <div class="col-lg-4 mt-5">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="header-title">Cari Motor Mu</h4>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <select v-model="kirim.id_merek" class="form-control">
                                                <option value>Pilih Merek</option>
                                                <option v-for="rowmerek in merek" :key="rowmerek.id" :value="rowmerek.id">
                                                    {{rowmerek.nama}}</option>
                                            </select>
                                        </div>
                                    </div>
                
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <select v-model="kirim.model" class="form-control">
                                                <option value>Pilih Model</option>
                                                <option v-for="rowmodel in model" :key="rowmodel.id" :value="rowmodel.id">
                                                    {{rowmodel.nama}}</option>
                                            </select>
                                        </div>
                                    </div>
                
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input v-model="kirim.tahun" type="number" min="1900" :max="new Date().getFullYear()"
                                                class="form-control" />
                                        </div>
                                    </div>
                
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select v-model="kirim.kondisi" class="form-control">
                                                <option value="baru">Baru</option>
                                                <option value="bekas">Bekas</option>
                                            </select>
                                        </div>
                                    </div>
                
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <select v-model="kirim.transmisi" class="form-control">
                                                <option value>Pilih Transmisi</option>
                                                <option v-for="rowtransmisi in transmisi" :key="rowtransmisi.id"
                                                    :value="rowtransmisi.id">{{rowtransmisi.transmisi}}</option>
                                            </select>
                                        </div>
                                    </div>
                
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <select v-model="kirim.cc" class="form-control">
                                                <option value>Pilih CC</option>
                                                <option value="90">90</option>
                                                <option value="100">100</option>
                                                <option value="110">110</option>
                                                <option value="125">125</option>
                                                <option value="150">150</option>
                                                <option value="250">250</option>
                                            </select>
                                        </div>
                                    </div>
                
                                    <button class="btn btn-block btn-primary">
                                        <i class="fa fa-search"></i> Cari Motor
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- main content area end -->
        <!-- footer area start-->
        <?php get_footer();?>