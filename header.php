<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>srtdash - SEO Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="<?= get_template_directory_uri();?>/assets/images/icon/favicon.ico">
    <link rel="stylesheet" href="<?= get_template_directory_uri();?>/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= get_template_directory_uri();?>/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= get_template_directory_uri();?>/assets/css/themify-icons.css">
    <link rel="stylesheet" href="<?= get_template_directory_uri();?>/assets/css/metisMenu.css">
    <link rel="stylesheet" href="<?= get_template_directory_uri();?>/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?= get_template_directory_uri();?>/assets/css/slicknav.min.css">
    <!-- amchart css -->
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    <!-- others css -->
    <link rel="stylesheet" href="<?= get_template_directory_uri();?>/assets/css/typography.css">
    <link rel="stylesheet" href="<?= get_template_directory_uri();?>/assets/css/default-css.css">
    <link rel="stylesheet" href="<?= get_template_directory_uri();?>/assets/css/styles.css">
    <link rel="stylesheet" href="<?= get_template_directory_uri();?>/assets/css/responsive.css">
    <!-- modernizr css -->
    <script src="<?= get_template_directory_uri();?>/assets/js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body class="body-bg">
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- preloader area start -->
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- preloader area end -->
    <!-- main wrapper start -->
    <div class="horizontal-main-wrapper">
        <!-- main header area start -->
        <div class="mainheader-area">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-3">
                        <div class="logo">
                            <a href="<?= bloginfo('siteurl');?>"><img src="<?= get_template_directory_uri();?>/assets/images/icon/logo2.png" alt="logo"></a>
                        </div>
                    </div>
                    <!-- profile info & task notification -->
                    <div class="col-md-9 clearfix text-right">
                        <div class="d-md-inline-block d-block mr-md-4">
                            <ul class="notification-area">
                                <li class="dropdown">
                                    <?php
                                        global $woocommerce;
                                    ?>
                                    <i class="fa fa-shopping-cart dropdown-toggle" data-toggle="dropdown"><span><?= $woocommerce->cart->get_cart_contents_count();?></span></i>
                                    <div class="dropdown-menu notify-box nt-enveloper-box">
                                        <div class="nofity-list">
                                            <?php $cart_items=$woocommerce->cart->get_cart();?>
                                            <?php foreach($cart_items as $item => $values):?>
                                                <?php
                                                    $product = wc_get_product( $values['data']->get_id() );
                                                    $getProductDetail = wc_get_product( $values['product_id'] );
                                                ?>
                                                <div class="notify-item">
                                                    <div class="notify-thumb">
                                                        <?= $product->get_image();?>
                                                    </div>
                                                    <div class="notify-text">
                                                        <p><?= $product->get_name();?></p>
                                                        <input type="number" name="qty[]" value="<?= $values['quantity'];?>" max="<?= $product->get_stock_quantity();?>" class="form-control w-50">
                                                        <span><?= $product->get_price_html();?></span>
                                                    </div>
                                                </div>
                                            <?php endforeach;?>
                                        </div>
                                        <span class="notify-title text-center"><a href="<?= bloginfo('siteurl').'/cart';?>" style="float:unset!important;">Check Out</a></span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="clearfix d-md-inline-block d-block">
                            <div class="user-profile m-0">
                                <img class="avatar user-thumb" src="<?= get_template_directory_uri();?>/assets/images/author/avatar.png" alt="avatar">
                                <h4 class="user-name dropdown-toggle" data-toggle="dropdown">Kumkum Rai <i class="fa fa-angle-down"></i></h4>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#">Message</a>
                                    <a class="dropdown-item" href="#">Settings</a>
                                    <a class="dropdown-item" href="#">Log Out</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- main header area end -->
        <!-- header area start -->
        <div class="header-area header-bottom">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-9  d-none d-lg-block">
                        <div class="horizontal-menu">
                            <nav>
                                <ul id="nav_menu">
                                    <li>
                                        <a href="javascript:void(0)"><i class="ti-dashboard"></i><span>dashboard</span></a>
                                        <ul class="submenu">
                                            <li><a href="index.html">ICO dashboard</a></li>
                                            <li><a href="index2.html">Ecommerce dashboard</a></li>
                                            <li><a href="index3.html">SEO dashboard</a></li>
                                        </ul>
                                    </li>
                                    <li class="active">
                                        <a href="javascript:void(0)"><i class="ti-layout-sidebar-left"></i><span>Sidebar
                                                Types</span></a>
                                        <ul class="submenu">
                                            <li><a href="index.html">Left Sidebar</a></li>
                                            <li class="active"><a href="index3-horizontalmenu.html">Horizontal Sidebar</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><i class="ti-pie-chart"></i><span>Charts</span></a>
                                        <ul class="submenu">
                                            <li><a href="barchart.html">bar chart</a></li>
                                            <li><a href="linechart.html">line Chart</a></li>
                                            <li><a href="piechart.html">pie chart</a></li>
                                        </ul>
                                    </li>
                                    <li class="mega-menu">
                                        <a href="javascript:void(0)"><i class="ti-palette"></i><span>UI Features</span></a>
                                        <ul class="submenu">
                                            <li><a href="accordion.html">Accordion</a></li>
                                            <li><a href="alert.html">Alert</a></li>
                                            <li><a href="badge.html">Badge</a></li>
                                            <li><a href="button.html">Button</a></li>
                                            <li><a href="button-group.html">Button Group</a></li>
                                            <li><a href="cards.html">Cards</a></li>
                                            <li><a href="dropdown.html">Dropdown</a></li>
                                            <li><a href="list-group.html">List Group</a></li>
                                            <li><a href="media-object.html">Media Object</a></li>
                                            <li><a href="modal.html">Modal</a></li>
                                            <li><a href="pagination.html">Pagination</a></li>
                                            <li><a href="popovers.html">Popover</a></li>
                                            <li><a href="progressbar.html">Progressbar</a></li>
                                            <li><a href="tab.html">Tab</a></li>
                                            <li><a href="typography.html">Typography</a></li>
                                            <li><a href="form.html">Form</a></li>
                                            <li><a href="grid.html">grid system</a></li>
                                        </ul>
                                    </li>
                                    <li class="mega-menu">
                                        <a href="javascript:void(0)"><i class="ti-layers-alt"></i> <span>Pages</span></a>
                                        <ul class="submenu">
                                            <li><a href="login.html">Login</a></li>
                                            <li><a href="login2.html">Login 2</a></li>
                                            <li><a href="login3.html">Login 3</a></li>
                                            <li><a href="register.html">Register</a></li>
                                            <li><a href="register2.html">Register 2</a></li>
                                            <li><a href="register3.html">Register 3</a></li>
                                            <li><a href="register4.html">Register 4</a></li>
                                            <li><a href="screenlock.html">Lock Screen</a></li>
                                            <li><a href="screenlock2.html">Lock Screen 2</a></li>
                                            <li><a href="reset-pass.html">reset password</a></li>
                                            <li><a href="pricing.html">Pricing</a></li>
                                            <li><a href="404.html">Error 404</a></li>
                                            <li><a href="500.html">Error 500</a></li>
                                            <li><a href="invoice.html"><i class="ti-receipt"></i> <span>Invoice Summary</span></a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><i class="ti-slice"></i><span>icons</span></a>
                                        <ul class="submenu">
                                            <li><a href="fontawesome.html">fontawesome icons</a></li>
                                            <li><a href="themify.html">themify icons</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><i class="fa fa-table"></i>
                                            <span>Tables</span></a>
                                        <ul class="submenu">
                                            <li><a href="table-basic.html">basic table</a></li>
                                            <li><a href="table-layout.html">table layout</a></li>
                                            <li><a href="datatable.html">datatable</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="maps.html"><i class="ti-map-alt"></i> <span>maps</span></a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <!-- nav and search button -->
                    <div class="col-lg-3 clearfix">
                        <div class="search-box">
                            <form action="#">
                                <input type="text" name="search" placeholder="Search..." required>
                                <i class="ti-search"></i>
                            </form>
                        </div>
                    </div>
                    <!-- mobile_menu -->
                    <div class="col-12 d-block d-lg-none">
                        <div id="mobile_menu"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- header area end -->