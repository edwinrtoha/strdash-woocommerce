<?php get_header();?>
    <div class="main-content-inner">
        <div class="container">
            <?php while ( have_posts() ) : the_post();?>
                <?php $product = new WC_Product(get_the_id());?>
                <div class="row">
                    <div class="col-lg-12 mt-5 katalog">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <?php
                                        $feature_img=$product->get_gallery_image_ids();
                                    ?>
                                    <?php if(sizeof($feature_img)>0):?>
                                        <div class="col-md-4">
                                            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                                <ol class="carousel-indicators">
                                                    <?php for ($i=0; $i < sizeof($feature_img); $i++):?>
                                                        <li data-target="#carouselExampleIndicators" data-slide-to="<?= $i;?>"<?= $i==1 ? '' : ' class="active"';?>></li>
                                                    <?php endfor;?>
                                                </ol>
                                                <div class="carousel-inner">
                                                    <?php for ($i=0; $i < sizeof($feature_img); $i++):?>
                                                        <div class="carousel-item<?= $i==1 ? '' : ' active';?>">
                                                            <img class="d-block w-100" src="<?php echo wp_get_attachment_image_src( $feature_img[$i], 'medium' )[0]; ?>"
                                                                alt="First slide" />
                                                        </div>
                                                    <?php endfor;?>
                                                </div>
                                                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                                                    data-slide="prev">
                                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                    <span class="sr-only">Previous</span>
                                                </a>
                                                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                                                    data-slide="next">
                                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                    <span class="sr-only">Next</span>
                                                </a>
                                            </div>
                                        </div>
                                    <?php endif;?>
                                    <div class="col-md-<?= sizeof($feature_img)>0 ? 8 : 12;?>">
                                        <h2><?= $product->get_name();?></h2>
                                        <h4><?= $product->get_price_html();?></h4>
                                        <input type="number" style="width: 5vw;" min="0" :max="Number(row.stok)" v-model="addcart.qty"
                                            class="form-control mb-3" @change="overStock()" />
                                        <a href="?add-to-cart=<?= $product->get_id();?>" class="btn btn-primary mr-3">Tambah Ke Keranjang</a>
                                        <button class="btn btn-warning">Nego</button>
                                        <h2>Spesifikasi</h2>
                                        <div class="table-responsive">
                                            <?php $attributes = $product->get_attributes();?>
                                            <table class="table table-striped">
                                                <?php foreach ($attributes as $attribute):?>
                                                    <tr>
                                                        <td><?= wc_attribute_label($attribute['name']);?></td>
                                                        <td><?= $product->get_attribute($attribute['name']);?></td>
                                                    </tr>
                                                <?php endforeach;?>
                                            </table>
                                        </div>
                                        <div class="deskripsi">
                                            <h2 class="mt-3">Deskripsi Produk</h2>
                                            <p><?= $product->get_description();?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Cari Motor end -->
                </div>
            <?php endwhile;?>
        </div>
    </div>
<?php get_footer();?>